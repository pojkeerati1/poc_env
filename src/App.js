import logo from './logo.svg';
import './App.css';

function App() {
  console.log('new version')
  console.log('show env data')
  console.log(process.env)
  console.log('print_env: ', process.env.REACT_APP_TEST)
  const env = process.env.REACT_APP_TEST
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          sam print2 env :{env}
        </a>
      </header>
    </div>
  );
}

export default App;
